<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Jenssegers\Mongodb\Eloquent\Model;
use MongoDB\BSON\ObjectId;
use MongoDB\BSON\UTCDateTime;

class Device extends Model
{
    use HasFactory;

    protected $collection = 'Devices';

    protected $fillable = [
        '_id',
        '_class',
        'channels',
        'dateCreate',
        'group',
        'icon',
        'location',
        'mac',
        'name',
        'options',
        'placeId',
        'qr',
        'ssid',
        'topicAlert',
        'type',
        'userId',
        'visible',
        'assignedDate',
        'ceco',
        'cn',
        'linkedDevices',
        'linked',
        'application',
        'is_cloud',
        'cloud_token',
        'cloud_url',
        'cloud_id'
    ];

    public static function initShellyRele(
        $mac,
        $name,
        $qr,
        $ssid,
        $cloud_token,
        $cloud_url,
        $cloud_id
    ) {
        $channels = [
            [
                '_id' => new ObjectId(),
                'label' => "Ultima Comunicaci\u00f3n",
                'topic' =>"rele\/lastupdate",
                'unit' => '',
            ],
            [
                '_id' => new ObjectId(),
                'label' => "Firmware",
                'topic' => '$fw\/version',
                'unit' => 'versi\u00f3n',
            ],
            [
                '_id' => new ObjectId(),
                'label' => "Se\u00f1al",
                'topic' =>"rele\/cobertura",
                'unit' => 'dBi',
            ]

        ];

        return Device::init(
            $mac,
            $name,
            $qr,
            $ssid,
            'rele/alert',
            4,
            true,
            $cloud_token,
            $cloud_url,
            $cloud_id,
            6,
            4,
            'Dispositivos',
            $channels
        );
    }

    public static function init(
        $mac,
        $name,
        $qr,
        $ssid,
        $topicAlert,
        $type,
        $is_cloud,
        $cloud_token,
        $cloud_url,
        $cloud_id,
        $group,
        $icon,
        $location,
        $channels = []
    ) {

       return Device::create([
            '_class' => 'com.sebastian.api.obj.devices.Device',
            'channels' => $channels,
            'dateCreate' => new UTCDateTime(),
            'mac' => $mac,
            'name' => $name,
            'options' => [],
            'qr' => $qr,
            'ssid' => $ssid,
            'topicAlert' => $topicAlert,
            'type' => $type,
            'visible' => true,
            'is_cloud' => $is_cloud,
            'cloud_token' => $cloud_token,
            'cloud_url' => $cloud_url,
            'cloud_id' => $cloud_id,
           'group' => $group,
           'icon' => $icon,
           'location' => $location,
        ]);

    }

}
