<?php

namespace App\Http\Controllers;

use App\Models\Device;
use Symfony\Component\HttpFoundation\Request;

class DeviceController extends Controller
{

    public function index() {
        $entities = Device::query()->paginate(20);

        return view('device.index', ['entities' => $entities]);
    }

    public function show($id) {
        $device = Device::find($id);

        return view('device.show', ['entity' => $device]);
    }

    public function create() {

        return view('device.create');
    }

    public function clone($id) {
        $device = Device::find($id);

        return view('device.create', ['entity' => $device]);
    }

    public function save(Request  $request) {

        $device = Device::init(
                $request->get('mac'),
                $request->get('name'),
                $request->get('qr'),
                $request->get('ssid'),
                $request->get('topicAlert'),
                $request->get('type'),
                $request->get('is_cloud'),
                $request->get('cloud_token'),
                $request->get('cloud_url'),
                $request->get('cloud_id')
        );

        return redirect(route('device.show', ['id' => $device->_id]));

    }

    public function createRele() {

        return view('device.create_rele');
    }

    public function saveRele(Request  $request) {

        $device = Device::initShellyRele(
            $request->get('mac'),
            $request->get('name'),
            $request->get('qr'),
            $request->get('ssid'),
            $request->get('cloud_token'),
            $request->get('cloud_url'),
            $request->get('cloud_id')
        );

        return redirect(route('device.show', ['id' => $device->_id]));
    }


}
