@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Device</div>

                    <div class="card-body show-entity">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <div class="item">
                            <div class="name">_id</div>
                            <div class="value">{{ $entity->_id }}</div>
                        </div>
                        <div class="item">
                            <div class="name">_class</div>
                            <div class="value">{{ $entity->_class }}</div>
                        </div>
                        <div class="item">
                            <div class="name">channels</div>
                            <pre><code>{{ json_encode($entity->channels, JSON_PRETTY_PRINT) }}</code></pre>
                        </div>
                        <div class="item">
                            <div class="name">dateCreated</div>
                            <div class="value">{{ $entity->dateCreate }}</div>
                        </div>
                        <div class="item">
                            <div class="name">group</div>
                            <div class="value">{{ $entity->group }}</div>
                        </div>
                        <div class="item">
                            <div class="name">icon</div>
                            <div class="value">{{ $entity->icon }}</div>
                        </div>
                        <div class="item">
                            <div class="name">location</div>
                            <div class="value">{{ $entity->location }}</div>
                        </div>
                        <div class="item">
                            <div class="name">mac</div>
                            <div class="value">{{ $entity->mac }}</div>
                        </div>
                        <div class="item">
                            <div class="name">name</div>
                            <div class="value">{{ $entity->name }}</div>
                        </div>
                        @if($entity->options)
                            <div class="item">
                                <div class="name">options</div>
                                <div class="value-array">
                                    <pre><code>{{ json_encode($entity->options, JSON_PRETTY_PRINT) }}</code></pre>
                                </div>
                            </div>
                        @endif
                        <div class="item">
                            <div class="name">placeId</div>
                            <div class="value">{{ $entity->placeId }}</div>
                        </div>
                        <div class="item">
                            <div class="name">qr</div>
                            <div class="value">{{ $entity->qr }}</div>
                        </div>
                        <div class="item">
                            <div class="name">ssid</div>
                            <div class="value">{{ $entity->ssid }}</div>
                        </div>
                        <div class="item">
                            <div class="name">topicAlert</div>
                            <div class="value">{{ $entity->topicAlert }}</div>
                        </div>
                        <div class="item">
                            <div class="name">type</div>
                            <div class="value">{{ $entity->type }}</div>
                        </div>
                        <div class="item">
                            <div class="name">userId</div>
                            <div class="value">{{ $entity->userId }}</div>
                        </div>
                        <div class="item">
                            <div class="name">visible</div>
                            <div class="value">{{ $entity->visible }}</div>
                        </div>
                        <div class="item">
                            <div class="name">assignedDate</div>
                            <div class="value">{{ $entity->assignedDate }}</div>
                        </div>
                        <div class="item">
                            <div class="name">ceco</div>
                            <div class="value">{{ $entity->ceco }}</div>
                        </div>
                        <div class="item">
                            <div class="name">cn</div>
                            <div class="value">{{ $entity->cn }}</div>
                        </div>
                        <div class="item">
                            <div class="name">linkedDevices</div>
                            <div class="value">{{ $entity->linkedDevices }}</div>
                        </div>
                        <div class="item">
                            <div class="name">linked</div>
                            <div class="value">{{ $entity->linked }}</div>
                        </div>
                        <div class="item">
                            <div class="name">application</div>
                            <div class="value">{{ $entity->application }}</div>
                        </div>
                        <div class="item">
                            <div class="name">is_cloud</div>
                            <div class="value">{{ $entity->is_cloud }}</div>
                        </div>
                        <div class="item">
                            <div class="name">cloud_token</div>
                            <div class="value">{{ $entity->cloud_token }}</div>
                        </div>
                        <div class="item">
                            <div class="name">cloud_url</div>
                            <div class="value">{{ $entity->cloud_url }}</div>
                        </div>

                        <div>
                            <a class="btn btn-primary" href="{{ route('device.index') }}">Back to list</a>
                            <a class="btn btn-primary" href="{{ route('device.clone', ['id' => $entity->id]) }}">Clone</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
