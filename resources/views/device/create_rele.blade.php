@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Create rele</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                            <form method="post" action="{{ route('device.create-rele__post') }}">

                            @csrf
                            {!! Form::text('mac', 'mac', isset($entity) ? $entity->mac : null) !!}
                            {!! Form::text('name', 'name', isset($entity) ? $entity->name : null) !!}
                            {!! Form::text('qr', 'qr', isset($entity) ? $entity->qr : null) !!}
                            {!! Form::text('ssid', 'ssid', isset($entity) ? $entity->ssid : null) !!}
                            {!! Form::text('cloud_token', 'cloud_token', isset($entity) ? $entity->cloud_token : null) !!}
                            {!! Form::text('cloud_url', 'cloud_url', isset($entity) ? $entity->cloud_url : null) !!}
                            {!! Form::text('cloud_id', 'cloud_id', isset($entity) ? $entity->cloud_id : null) !!}


                            {!!Form::submit("Create")!!}

                            </form>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
