@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Devices</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <div>
                            <a href="{{ route('device.create') }}">Create new</a>
                            <a href="{{ route('device.create-rele') }}">Create new rele</a>
                        </div>

                        <table class="table table-bordered">
                            <thead>

                            <tr>
                                <th>#</th>
                                <th>mac</th>
                                <th>qr</th>
                            </tr>
                            </thead>

                            <tbody>
                                @foreach($entities as $entity)
                                    <tr>
                                        <td><a href="{{ route('device.show', ['id' => $entity->_id]) }}">{{ $entity->_id }}</a> </td>
                                        <td>{{ $entity->mac }}</td>
                                        <td>{{ $entity->qr }}</td>
                                    </tr>
                                @endforeach
                            </tbody>

                        </table>

                            <div class="pagination">
                                {{ $entities->links() }}
                            </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
