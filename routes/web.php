<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/test', [\App\Http\Controllers\HomeController::class, 'index']);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/device', [App\Http\Controllers\DeviceController::class, 'index'])->name('device.index');
Route::get('/device/create-rele', [App\Http\Controllers\DeviceController::class, 'createRele'])->name('device.create-rele');
Route::post('/device/create-rele', [App\Http\Controllers\DeviceController::class, 'saveRele'])->name('device.create-rele__post');
Route::get('/device/create', [App\Http\Controllers\DeviceController::class, 'create'])->name('device.create');
Route::post('/device/create', [App\Http\Controllers\DeviceController::class, 'save'])->name('device.create__post');
Route::get('/device/clone/{id}', [App\Http\Controllers\DeviceController::class, 'clone'])->name('device.clone');
Route::get('/device/{id}', [App\Http\Controllers\DeviceController::class, 'show'])->name('device.show');


